api = 2
core = 7.x
projects[drupal][version] = 7.16

; see http://drupal.org/node/1074108
projects[drupal][patch][] = "http://drupal.org/files/issues/1074108-skip-profile-2.patch"

; see http://drupal.org/node/1681542#comment-6226992
projects[drupal][patch][] = "http://drupal.org/files/159141-1.patch"

; see http://drupal.org/node/728702#comment-5045334
projects[drupal][patch][] = "http://drupal.org/files/issues/install-redirect-on-empty-database-728702-36.patch"