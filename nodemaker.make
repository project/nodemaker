; Development makefile for NodeMaker
; This should ONLY be used for development purposes
; Pulls most ThemeGeeks controlled themes/modules in via git
api = 2
core = 7.x
projects[drupal][version] = 7.16

; Core patches
; see http://drupal.org/node/1074108
projects[drupal][patch][] = "http://drupal.org/files/issues/1074108-skip-profile-2.patch"
; see http://drupal.org/node/1681542#comment-6226992
projects[drupal][patch][] = "http://drupal.org/files/159141-1.patch"
; see http://drupal.org/node/728702#comment-5045334
projects[drupal][patch][] = "http://drupal.org/files/issues/install-redirect-on-empty-database-728702-36.patch"

; Themes & Theme Related Tools
projects[omega][download][type] = "git"
projects[omega][download][url] = "http://git.drupal.org/project/omega.git"
projects[omega][download][branch] = "7.x-3.x"

projects[omega_nodemaker][download][type] = "git"
projects[omega_nodemaker][download][url] = "git@github.com:ThemeGeeks/omega_nodemaker.git"
projects[omega_nodemaker][download][branch] = "7.x-3.x"

projects[omega_tools][subdir] = "contrib"

; Nodemaker Apps
projects[nm_core][download][type] = "git"
projects[nm_core][type] = "module"
projects[nm_core][download][url] = "git@github.com:ThemeGeeks/nm_core.git"
projects[nm_core][download][branch] = "7.x-1.x"
projects[nm_core][subdir] = "apps"

projects[nm_blog][download][type] = "git"
projects[nm_blog][type] = "module"
projects[nm_blog][download][url] = "git@github.com:ThemeGeeks/nm_blog.git"
projects[nm_blog][download][branch] = "7.x-1.x"
projects[nm_blog][subdir] = "apps"

projects[nm_events][download][type] = "git"
projects[nm_events][type] = "module"
projects[nm_events][download][url] = "git@github.com:ThemeGeeks/nm_events.git"
projects[nm_events][download][branch] = "7.x-1.x"
projects[nm_events][subdir] = "apps"

projects[nm_galleries][download][type] = "git"
projects[nm_galleries][type] = "module"
projects[nm_galleries][download][url] = "git@github.com:ThemeGeeks/nm_galleries.git"
projects[nm_galleries][download][branch] = "7.x-1.x"
projects[nm_galleries][subdir] = "apps"

projects[nm_announcements][download][type] = "git"
projects[nm_announcements][type] = "module"
projects[nm_announcements][download][url] = "git@github.com:ThemeGeeks/nm_announcements.git"
projects[nm_announcements][download][branch] = "7.x-1.x"
projects[nm_announcements][subdir] = "apps"

projects[nm_reviews][download][type] = "git"
projects[nm_reviews][type] = "module"
projects[nm_reviews][download][url] = "git@github.com:ThemeGeeks/nm_reviews.git"
projects[nm_reviews][download][branch] = "7.x-1.x"
projects[nm_reviews][subdir] = "apps"

projects[nm_testimonials][download][type] = "git"
projects[nm_testimonials][type] = "module"
projects[nm_testimonials][download][url] = "git@github.com:ThemeGeeks/nm_testimonials.git"
projects[nm_testimonials][download][branch] = "7.x-1.x"
projects[nm_testimonials][subdir] = "apps"

projects[nm_teams][download][type] = "git"
projects[nm_teams][type] = "module"
projects[nm_teams][download][url] = "git@github.com:ThemeGeeks/nm_teams.git"
projects[nm_teams][download][branch] = "7.x-1.x"
projects[nm_teams][subdir] = "apps"

projects[nm_socialnetwork][download][type] = "git"
projects[nm_socialnetwork][type] = "module"
projects[nm_socialnetwork][download][url] = "git@github.com:ThemeGeeks/nm_socialnetwork.git"
projects[nm_socialnetwork][download][branch] = "7.x-1.x"
projects[nm_socialnetwork][subdir] = "apps"

; NodeMaker contrib modules
projects[views][subdir] = "contrib"
projects[imagecrop][version] = "1.x"
projects[imagecrop][subdir] = "contrib"
projects[features][subdir] = "contrib"
projects[ctools][subdir] = "contrib"
projects[context][subdir] = "contrib"
projects[boxes][subdir] = "contrib"
projects[globalredirect][subdir] = "contrib"
projects[transliteration][subdir] = "contrib"
projects[navigation404][subdir] = "contrib"
projects[block_class][subdir] = "contrib"
projects[extlink][subdir] = "contrib"
projects[token][subdir] = "contrib"
projects[pathauto][subdir] = "contrib"
projects[delta][subdir] = "contrib"
projects[google_analytics][subdir] = "contrib"
projects[strongarm][subdir] = "contrib"
projects[diff][subdir] = "contrib"
projects[field_group][subdir] = "contrib"
projects[flexslider][subdir] = "contrib"
projects[libraries][subdir] = "contrib"
projects[profile2][subdir] = "contrib"
projects[logintoboggan][subdir] = "contrib"
projects[menu_expanded][subdir] = "contrib"
projects[menu_block][subdir] = "contrib"
projects[nice_menus][subdir] = "contrib"
projects[colorbox][subdir] = "contrib"
projects[entity][subdir] = "contrib"
projects[entityreference][subdir] = "contrib"
projects[date][subdir] = "contrib"
projects[calendar][subdir] = "contrib"
projects[link][subdir] = "contrib"
projects[addressfield][subdir] = "contrib"
projects[addressfield_staticmap][subdir] = "contrib"
projects[taxonomy_menu_trails][subdir] = "contrib"
projects[metatag][subdir] = "contrib"
projects[rules][subdir] = "contrib"
projects[auto_nodetitle][subdir] = "contrib"
projects[email][subdir] = "contrib"
projects[votingapi][subdir] = "contrib"
projects[activity][subdir] = "contrib"
projects[statuses][subdir] = "contrib"
projects[module_filter][subdir] = "contrib"
projects[fivestar][subdir] = "contrib"
projects[captcha][subdir] = "contrib"
projects[sharethis][subdir] = "contrib"
projects[better_exposed_filters][subdir] = "contrib"
projects[filefield_sources][subdir] = "contrib"
projects[filefield_sources_plupload][subdir] = "contrib"
projects[plupload][subdir] = "contrib"

; Development Modules
projects[devel][subdir] = "development"

; Libraries
libraries[flexslider][type] = "libraries"
libraries[flexslider][download][type] = "git"
libraries[flexslider][download][url] = "https://github.com/woothemes/FlexSlider.git"
libraries[flexslider][download][branch] = "flexslider1"
libraries[flexslider][directory_name] = "flexslider"

libraries[colorbox][type] = "libraries"
libraries[colorbox][download][type] = "file"
libraries[colorbox][download][url] = "http://jacklmoore.com/colorbox/colorbox.zip"
libraries[colorbox][directory_name] = "colorbox"

libraries[plupload][type] = "libraries"
libraries[plupload][download][type] = "file"
libraries[plupload][download][url] = "https://github.com/downloads/moxiecode/plupload/plupload_1_5_4.zip"
libraries[plupload][directory_name] = "plupload"

; Additional patches
; see http://drupal.org/node/1506582#comment-6012720
; projects[fivestar][patch][] = "http://drupal.org/files/rating_not_showing_block_on_site-1506582-854220.patch"
; see http://drupal.org/node/1276820#comment-4989074
projects[profile2][patch][] = "http://drupal.org/files/profile2-add_devel_generate_for_profile2_content-1276820-20.patch"