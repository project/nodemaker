<?php

function nodemaker_app_installer($app, $name, &$context) {
  $context['message'] = t('Finished Installing: '. $name);
  module_enable(array($app));
}


function _nodemaker_finalize_variables(&$context) {
  $context['message'] = t('Saving some variables');

  //set better date format
  variable_set('date_format_nodemaker_date_friendly', 'l, F jS');

  $apps = variable_get('nodemaker_install_apps');
  $install_apps = $apps['nodemaker_default_apps'];

  if (isset($install_apps['nm_landing_pages']) && $install_apps['nm_landing_pages'] == 1) {
    //save custom home page
    variable_set('site_frontpage', 'landing/nm_core');
  }
}


function _nodemaker_finalize_remove_variables(&$context) {
  $context['message'] = t('Cleaning up app installation variables...');
  variable_del('nodemaker_install_apps');
}


/**
 * Resave user/1 so that it gets pathauto aliases that didn't exist at time of
 * first account creation
 */
function _nodemaker_finalize_resave_user1(&$context) {
  $context['message'] = t('Setting paths for user account...');
  $account = user_load(1, TRUE);
  user_save($account);
}


/**
 * Create a user for each role
 */
function _nodemaker_finalize_create_users(&$context) {
  $context['message'] = t('Creating users for each role...');

  $site_mail = variable_get('site_mail');
  $mail_parts = explode('@', $site_mail);
  $domain = $mail_parts[1];

  $roles = db_select('role', 'r')
    ->fields('r', array('rid', 'name'))
    ->condition('name', array('anonymous user', 'authenticated user'), 'NOT IN')
    ->execute()->fetchAll();

  foreach ($roles as $edit) {
    $account = new StdClass();
    $account->is_new = TRUE;
    $edit->roles = array($edit->rid => $edit->rid);
    $edit->mail = 'role-' . str_replace(' ', '-', $edit->name) . '@' . $domain;
    $edit->status = TRUE;
    $edit->language = 'und';
    unset($edit->rid);
    $user_data = (array) $edit;
    user_save($account, $user_data);
  }
}


function _nodemaker_finalize_caches(&$context) {
  $context['message'] = t('Flushing Drupal cache...');
  drupal_flush_all_caches();
}