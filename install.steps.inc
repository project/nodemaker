<?php

/**
 * Implements hook_form_FORM_ID_alter().
 *
 * Allows the profile to alter the site configuration form.
 */
function nodemaker_form_install_configure_form_alter(&$form, $form_state) {
  // Since any module can add a drupal_set_message, this can bug the user
  // when we display this page. For a better user experience,
  // remove all the messages.
  drupal_get_messages(NULL, TRUE);

  // Set a default name for the dev site and change title's label.
  $form['site_information']['site_name']['#title'] = 'Website name';
  $form['site_information']['site_mail']['#title'] = 'Website email address';
  $form['site_information']['site_name']['#default_value'] = t('NodeMaker');

  // Set a default country so we can benefit from it on Address Fields.
  $form['server_settings']['site_default_country']['#default_value'] = 'US';

  // Use "admin" as the default username.
  $form['admin_account']['account']['name']['#default_value'] = 'admin';

  // Set the default admin password.
  $form['admin_account']['account']['pass']['#value'] = 'admin';

  // Hide Update Notifications.
  $form['update_notifications']['#access'] = FALSE;

  // Add informations about the default username and password.
  $form['admin_account']['account']['nodemaker_name'] = array(
    '#type' => 'item', '#title' => st('Username'),
    '#markup' => 'admin'
  );
  $form['admin_account']['account']['nodemaker__password'] = array(
    '#type' => 'item', '#title' => st('Password'),
    '#markup' => 'admin'
  );
  $form['admin_account']['account']['nodemaker_informations'] = array(
    '#markup' => '<p>' . t('You will be able to change this later.') . '</p>'
  );
  $form['admin_account']['override_account_informations'] = array(
    '#type' => 'checkbox',
    '#title' => t('Change my username and password.'),
  );
  $form['admin_account']['setup_account'] = array(
    '#type' => 'container',
    '#parents' => array('admin_account'),
    '#states' => array(
      'invisible' => array(
        'input[name="override_account_informations"]' => array('checked' => FALSE),
      ),
    ),
  );

  // Make a "copy" of the original name and pass form fields.
  $form['admin_account']['setup_account']['account']['name'] = $form['admin_account']['account']['name'];
  $form['admin_account']['setup_account']['account']['pass'] = $form['admin_account']['account']['pass'];

  // Use "admin" as the default username.
  $form['admin_account']['account']['name']['#default_value'] = 'admin';
  $form['admin_account']['account']['name']['#access'] = FALSE;

  // Set the default admin password.
  $form['admin_account']['account']['pass']['#value'] = 'admin';

  // Make the password "hidden".
  $form['admin_account']['account']['pass']['#type'] = 'hidden';
  $form['admin_account']['account']['mail']['#access'] = FALSE;
  
  // Add a custom validation that needs to be trigger before the original one,
  // where we can copy the site's mail as the admin account's mail.
  array_unshift($form['#validate'], 'nodemaker_custom_setting');
}

/**
 * Validate callback; Populate the admin account mail, user and password with
 * custom values.
 */
function nodemaker_custom_setting(&$form, &$form_state) {
  $form_state['values']['account']['mail'] = $form_state['values']['site_mail'];
  // Use our custom values only the corresponding checkbox is checked.
  if ($form_state['values']['override_account_informations'] == TRUE) {
    if ($form_state['input']['pass']['pass1'] == $form_state['input']['pass']['pass2']) {
      $form_state['values']['account']['name'] = $form_state['values']['name'];
      $form_state['values']['account']['pass'] = $form_state['input']['pass']['pass1'];
    }
  else {
      form_set_error('pass', t('The specified passwords do not match.'));
    }
  }
}

/**
 * Set contact form 'to' email
 */
function nodemaker_install_contact() {
  $site_mail = variable_get('site_mail', FALSE);
  db_update('contact')
    ->fields(array(
      'recipients' => $site_mail,
    ))
    ->condition('cid',1)
    ->execute();
}


function nodemaker_configure_apps($form, &$form_state) {
  //clear any messages
  drupal_get_messages(NULL, TRUE);

  $form = array();
  /**
   * Default Content & Apps
   */
  $form['#tree'] = TRUE;
  $form['nodemaker_load_default_apps'] = array(
    '#type' => 'radios',
    '#title' => t('Install NodeMaker Apps'),
    '#description' => t('<p>NodeMaker provides a great set of default "Apps" that can help power your site with advanced features like a multi-user blog, event calendar, photo galleries and advanced member profiles. It is recommended to use these apps as your starting point for further customization. Enabling all the features is highly recommended for most users.</p>'),
    '#options' =>  array(
      'enabled' => t('Enable all Apps <strong><small>(RECOMMENDED)</small></strong>'), 
      'disabled' => t('Disable all Apps'), 
      'customized' => t('Customize Apps')),
    '#default_value' => 'enabled',
  );
  
  $form['nodemaker_default_apps'] = array(
    '#type' => 'fieldset',
    '#states' => array(
      // Only enable this field when the Customize radio option is selected.
      'visible' => array(
        ':input[name="nodemaker_load_default_apps"]' => array('value' => 'customized'),
      ),
    ),
  );
  
  
  $form['nodemaker_load_default_content'] = array(
    '#type' => 'radios',
    '#title' => t('Install Default Content'),
    '#description' => t('<p>Default Content is provided by the NodeMaker distribution so that when you install your site, you can see the full functionality at work!</p><p>Here you can customize the default content that is installed by default in your NodeMaker website. This content is enabled by default as it greatly demonstrates the functionality of the site, and ensures that you see what you are working with when building and customizing your own site. You may disable this content, or select specific packages to enable or disable.</p>'),
    '#options' =>  array(
      'enabled' => t('Enable all Content <strong><small>(RECOMMENDED)</small></strong>'), 
      'disabled' => t('Disable all Content'), 
      'customized' => t('Customize Content Packages')),
    '#default_value' => 'enabled',
    '#states' => array(
      // Only enable this field when the Customize radio option is selected.
      'invisible' => array(
        ':input[name="nodemaker_load_default_apps"]' => array('value' => 'disabled'),
      ),
    ),
  );

  $form['nodemaker_default_content'] = array(
    '#type' => 'fieldset',
    '#states' => array(
      // Only enable this field when the Customize radio option is selected.
      'visible' => array(
        ':input[name="nodemaker_load_default_content"]' => array('value' => 'customized'),
      ),
    ),
  );

  //show NodeMaker feature apps
  $apps = nodemaker_get_modules('apps', array(0,1));
  foreach ($apps as $module => $info) {
   //make a checkbox
    $form['nodemaker_default_apps'][$module] = array(
      '#type' => 'checkbox',
      '#title' => t($info['name']),
      '#description' => t($info['description']),
      '#default_value' => 1,
      '#states' => array(
        // Only enable this field when the Customize radio option is selected.
        'unchecked' => array(
          ':input[name="nodemaker_load_default_apps"]' => array('value' => 'disabled'),
        ),
        'enabled' => array(
          ':input[name="nodemaker_load_default_apps"]' => array('value' => 'customized'),
        ),
      ),
    );
  }

  //show NodeMaker content apps
  $contents = nodemaker_get_modules('content', array(0,1));
  foreach ($contents as $module => $info) {
    //make a checkbox
    $form['nodemaker_default_content'][$module] = array(
      '#type' => 'checkbox',
      '#title' => t($info['name']),
      '#description' => t($info['description']),
      '#default_value' => 1,
      '#states' => array(
        // Only enable this field when the Customize radio option is selected.
        'unchecked' => array(
          ':input[name="nodemaker_load_default_content"]' => array('value' => 'disabled'),
        ),
        'enabled' => array(
          ':input[name="nodemaker_load_default_content"]' => array('value' => 'customized'),
        ),
        'visible' => array(
          ':input[name="' . str_replace('_default_content', '', $module) . '"]' => array('checked' => true),
        ),
      ),
    ); 
  }
  
  // submit button
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}


function nodemaker_configure_apps_validate($form, &$form_state) {
  // get rid of the ugly stuff in the $form_state['values'] array
  unset($form_state['values']['submit'], $form_state['values']['form_build_id'], $form_state['values']['form_token'], $form_state['values']['form_id'], $form_state['values']['op']);
}

/**
 *
 */
function nodemaker_configure_apps_submit($form, &$form_state) {
  variable_set('nodemaker_install_apps', $form_state['values']);
}




/**
 * Batch callback to install NodeMaker Apps during installation
 */
function nodemaker_install_apps() {
  //clear any messages
  drupal_get_messages(NULL, TRUE);
  
  //get all nodemaker modules
  
  
  $apps = variable_get('nodemaker_install_apps');
  $install_apps = $apps['nodemaker_default_apps'];
  $install_content = $apps['nodemaker_default_content'];
  
  $apps_init_msg = t('Preparing to install some awesome...');
  
  switch ($apps['nodemaker_load_default_apps']) {
    case 'enabled': 
      // all apps are to be enabled. just do it.
      
      $batch = array(
        'title' => t('Installing NodeMaker Apps'),
        'operations' => array(

        ),
        'init_message' => $apps_init_msg,
      );
      
      foreach ($install_apps AS $module => $v) {
        $apps_query = db_select('system', 's')
          ->fields('s', array('name', 'info'))
          ->condition('name', $module, 'LIKE')
          ->execute();
        $apps_result = $apps_query->fetch();
        $apps_info = unserialize($apps_result->info);
        $batch['operations'][] = array('nodemaker_app_installer', array($module, $apps_info['name']));  
      }

      return $batch;
      
    break;
    
    case 'disabled':
      // no apps are to be enabled. snap...
      // Still need to enable nm_core though
      $batch['title'] = t('Installing NodeMaker Core App (required)');
      $batch['init_message'] = t('Preparing to install some awesome...');
      $batch['operations'][] = array('nodemaker_app_installer', array('nm_core', t('NodeMaker Core')));

      return $batch;
    break;
    
    case 'customized':
      // we need to look over the actual checked options
      
      $batch = array(
        'title' => t('Installing NodeMaker Apps'),
        'operations' => array(),
        'init_message' => $apps_init_msg,
      );
      
      // make sure someone didn't deselect nm_core
      if (!$install_apps['nm_core']) {
        $batch['operations'][] = array('nodemaker_app_installer', array('nm_core', t('NodeMaker Core')));
      }
      
      foreach ($install_apps AS $module => $v) {
        if ($v) {
          $apps_query = db_select('system', 's')
            ->fields('s', array('name', 'info'))
            ->condition('name', $module, 'LIKE')
            ->execute();
          $apps_result = $apps_query->fetch();
          $apps_info = unserialize($apps_result->info);
          $batch['operations'][] = array('nodemaker_app_installer', array($module, $apps_info['name']));
        }
      }
      
      return $batch;
    break;
  }
}

/**
 * Batch callback to install NodeMaker Apps during installation
 */
function nodemaker_install_content() {
  //clear any messages
  drupal_get_messages(NULL, TRUE);
  
  //clear caches
  drupal_flush_all_caches();

  // get all nodemaker content modules
  $apps = variable_get('nodemaker_install_apps');
  
  $install_apps = $apps['nodemaker_default_apps'];
  $install_content = $apps['nodemaker_default_content'];
  
  $content_init_msg = t('Preparing to add a hawt bit of demo content to your site...');

  //if user selected disable for apps, then content should be disable as well
  if ($apps['nodemaker_load_default_apps'] == 'disabled') {
    $apps['nodemaker_load_default_content'] = 'disabled';
  }
  
  switch ($apps['nodemaker_load_default_content']) {
    case 'enabled':

      $batch = array(
        'title' => t('Installing Content'),
        'operations' => array(

        ),
        'init_message' => $content_init_msg,
      );
      
      foreach($install_content as $module => $v) {
        // check to see that any dependencies were met
        $parent = str_replace('_default_content', '', $module);
        if (module_exists($parent)) {
          $content_query = db_select('system', 's')
            ->fields('s', array('name', 'info'))
            ->condition('name', $module, 'LIKE')
            ->execute();
          $content_result = $content_query->fetch();
          $content_info = unserialize($content_result->info);
          $batch['operations'][] = array('nodemaker_app_installer', array($module, $content_info['name']));    
        }
      }
      
      return $batch;
      
    break;
    
    case 'disabled':
      // do nothing (for now)
      return;
    break;
    
    case 'customized':
      
      $batch = array(
        'title' => t('Installing Content'),
        'operations' => array(

        ),
        'init_message' => $content_init_msg,
      );
    
      foreach($install_content as $module => $v) {
        if ($v) {
          // check to see that any dependencies were met
          $parent = str_replace('_default_content', '', $module);
          if (module_exists($parent)) {
            $content_query = db_select('system', 's')
              ->fields('s', array('name', 'info'))
              ->condition('name', $module, 'LIKE')
              ->execute();
            $content_result = $content_query->fetch();
            $content_info = unserialize($content_result->info);
            $batch['operations'][] = array('nodemaker_app_installer', array($module, $content_info['name']));    
          } 
        }
      }
      
      return $batch;
      
    break;
  }
}



/**
 * Final callback before redirecting to the website.
 * This is where any cleanup and final configuration adjustments will happen
 */
function nodemaker_finalize_install() {
  //clear any messages
  drupal_get_messages(NULL, TRUE);

  $final_init_msg = t('Adding a final dash of #awesomesauce...');

  $batch = array(
    'title' => t('Finalizing NodeMaker Installation'),
    'operations' => array(
    ),
    'init_message' => $final_init_msg,
  );

  // set any needed variables
  $batch['operations'][] = array('_nodemaker_finalize_variables', array());
  // remove any stray variables from install
  $batch['operations'][] = array('_nodemaker_finalize_remove_variables', array());
  // resave user1 to get aliases
  $batch['operations'][] = array('_nodemaker_finalize_resave_user1', array());
  //create a user for each role
  $batch['operations'][] = array('_nodemaker_finalize_create_users', array());

  //resave alises
  if (module_exists('pathauto')) {
    //delete term aliases
    db_delete('url_alias')
      ->condition('source', db_like('taxonomy/term/') . '%', 'LIKE')
      ->execute();
    //delete forum aliases
    db_delete('url_alias')
      ->condition('source', db_like('forum/') . '%', 'LIKE')
      ->execute();

    //make sure strongarm set vars
    variable_set('pathauto_taxonomy_term_nm_blog_category_pattern', 'blog/[term:name]');
    variable_set('pathauto_taxonomy_term_nm_announcement_category_pattern', 'announcements/[term:name]');
    variable_set('pathauto_forum_pattern', '[term:vocabulary]/[term:parents:join:/]/[term:name]');
    //add batch from pathauto
    module_load_include('inc', 'pathauto', 'pathauto.pathauto');
    $batch['operations'][] = array('taxonomy_pathauto_bulk_update_batch_process', array());
    $batch['operations'][] = array('forum_pathauto_bulk_update_batch_process', array());
    $batch['file'] = drupal_get_path('module', 'pathauto') . '/pathauto.pathauto.inc';
  }
  
  return $batch;
}