; Drupal.org release file.
core = 7.16
api = 2

; Themes & Theme Related Tools
projects[omega][version] = 3.1
projects[omega_nodemaker][version] = 3.x
projects[omega_tools][subdir] = "contrib"

; Nodemaker Apps

; Core functionality
projects[nm_core][subdir] = "apps"
projects[nm_core][version] = "1.x"

; Blog
projects[nm_blog][subdir] = "apps"
projects[nm_blog][version] = "1.x"

; Events
projects[nm_events][subdir] = "apps"
projects[nm_events][version] = "1.x"

; Galleries
projects[nm_galleries][subdir] = "apps"
projects[nm_galleries][version] = "1.x"

; Announcements
projects[nm_announcements][subdir] = "apps"
projects[nm_announcements][version] = "1.x"

; Reviews (slated for post launch release)
;projects[nm_reviews][subdir] = "apps"
;projects[nm_reviews][version] = "1.x"

; Testimonials
projects[nm_testimonials][subdir] = "apps"
projects[nm_testimonials][version] = "1.x"

; Teams
projects[nm_teams][subdir] = "apps"
projects[nm_teams][version] = "1.x"

; Social Network
projects[nm_socialnetwork][subdir] = "apps"
projects[nm_socialnetwork][version] = "1.x"

; NodeMaker contrib modules
projects[views][subdir] = "contrib"
projects[imagecrop][version] = "1.x"
projects[imagecrop][subdir] = "contrib"
projects[features][subdir] = "contrib"
projects[ctools][subdir] = "contrib"
projects[context][subdir] = "contrib"
projects[boxes][subdir] = "contrib"
projects[globalredirect][subdir] = "contrib"
projects[transliteration][subdir] = "contrib"
projects[navigation404][subdir] = "contrib"
projects[block_class][subdir] = "contrib"
projects[extlink][subdir] = "contrib"
projects[token][subdir] = "contrib"
projects[pathauto][subdir] = "contrib"
projects[delta][subdir] = "contrib"
projects[google_analytics][subdir] = "contrib"
projects[strongarm][subdir] = "contrib"
projects[diff][subdir] = "contrib"
projects[field_group][subdir] = "contrib"
projects[flexslider][subdir] = "contrib"
projects[libraries][subdir] = "contrib"
projects[profile2][subdir] = "contrib"
projects[logintoboggan][subdir] = "contrib"
projects[menu_expanded][subdir] = "contrib"
projects[menu_block][subdir] = "contrib"
projects[nice_menus][subdir] = "contrib"
projects[colorbox][subdir] = "contrib"
projects[entity][subdir] = "contrib"
projects[entityreference][subdir] = "contrib"
projects[date][subdir] = "contrib"
projects[calendar][subdir] = "contrib"
projects[link][subdir] = "contrib"
projects[addressfield][subdir] = "contrib"
projects[addressfield_staticmap][subdir] = "contrib"
projects[taxonomy_menu_trails][subdir] = "contrib"
projects[metatag][subdir] = "contrib"
projects[rules][subdir] = "contrib"
projects[auto_nodetitle][subdir] = "contrib"
projects[email][subdir] = "contrib"
projects[votingapi][subdir] = "contrib"
projects[activity][subdir] = "contrib"
projects[statuses][subdir] = "contrib"
projects[module_filter][subdir] = "contrib"
projects[fivestar][subdir] = "contrib"
projects[captcha][subdir] = "contrib"
projects[sharethis][subdir] = "contrib"
projects[better_exposed_filters][subdir] = "contrib"

projects[filefield_sources][subdir] = "contrib"
projects[filefield_sources_plupload][subdir] = "contrib"
projects[plupload][subdir] = "contrib"


; Development Modules
projects[devel][subdir] = "development"

; Libraries
libraries[flexslider][type] = "libraries"
libraries[flexslider][download][type] = "git"
libraries[flexslider][download][url] = "https://github.com/woothemes/FlexSlider.git"
libraries[flexslider][download][branch] = "flexslider1"
libraries[flexslider][directory_name] = "flexslider"

libraries[colorbox][type] = "libraries"
libraries[colorbox][download][type] = "file"
libraries[colorbox][download][url] = "http://jacklmoore.com/colorbox/colorbox.zip"
libraries[colorbox][directory_name] = "colorbox"

libraries[plupload][type] = "libraries"
libraries[plupload][download][type] = "file"
libraries[plupload][download][url] = "https://github.com/downloads/moxiecode/plupload/plupload_1_5_4.zip"
libraries[plupload][directory_name] = "plupload"

; Additional patches
; see http://drupal.org/node/1506582#comment-6012720
; projects[fivestar][patch][] = "http://drupal.org/files/rating_not_showing_block_on_site-1506582-854220.patch"
; see http://drupal.org/node/1276820#comment-4989074
projects[profile2][patch][] = "http://drupal.org/files/profile2-add_devel_generate_for_profile2_content-1276820-20.patch"