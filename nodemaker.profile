<?php

require_once(drupal_get_path('profile', 'nodemaker') . '/install.functions.inc');
require_once(drupal_get_path('profile', 'nodemaker') . '/install.steps.inc');


/**
 * Implements hook_theme().
 */
function nodemaker_theme($existing, $type, $theme, $path) {
  return array(
    'toggleswitch' => array(
      'render element' => 'element',
    ),
  );
}

/**
 * Implements hook_menu.
 */
function nodemaker_menu() {

  //settings page
  $items['admin/config/nodemaker'] = array(
    'title' => 'NodeMaker',
    'description' => 'Settings for any of the NodeMaker apps.',
    'page callback' => 'system_admin_menu_block_page',
    'access arguments' => array('access administration pages'),
    'file' => 'system.admin.inc',
    'file path' => drupal_get_path('module', 'system'),
  );


  //check if we should hide the welcome screen
  if (variable_get('nodemaker_hide_welcome', 0) == 1) {
    $items['admin/nodemaker'] = array(
      'title' => t('NodeMaker'),
      'description' => t('Manage your NodeMaker website.'),
      'page callback' => 'nodemaker_apps',
      'access arguments' => array('administer site configuration'),
      'file' => 'nodemaker.pages.inc',
      'type' => MENU_NORMAL_ITEM,
      'weight' => -100,
    );
    $items['admin/nodemaker/apps'] = array(
      'title' => t('Apps'),
      'description' => t('Manage your NodeMaker website.'),
      'page callback' => 'nodemaker_apps',
      'access arguments' => array('administer site configuration'),
      'file' => 'nodemaker.pages.inc',
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -100,
    );
    $items['admin/nodemaker/apps/apps'] = array(
      'title' => t('Apps'),
      'description' => t('Manage your NodeMaker website.'),
      'page callback' => 'nodemaker_apps',
      'access arguments' => array('administer site configuration'),
      'file' => 'nodemaker.pages.inc',
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -100,
    );
  }

  elseif (variable_get('nodemaker_hide_welcome', 0) == 0) {
    $items['admin/nodemaker'] = array(
      'title' => t('Getting Started'),
      'description' => t('Understanding and using your NodeMaker website.'),
      'page callback' => 'nodemaker_welcome',
      'access arguments' => array('administer site configuration'),
      'file' => 'nodemaker.pages.inc',
      'type' => MENU_NORMAL_ITEM,
      'weight' => -100,
    );
    $items['admin/nodemaker/welcome'] = array(
      'title' => t('Getting Started'),
      'page callback' => 'nodemaker_welcome',
      'access arguments' => array('administer site configuration'),
      'file' => 'nodemaker.pages.inc',
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -100,
    );
    $items['admin/nodemaker/apps'] = array(
      'title' => t('NodeMaker Apps'),
      'page callback' => 'nodemaker_apps',
      'access arguments' => array('administer site configuration'),
      'file' => 'nodemaker.pages.inc',
      'type' => MENU_LOCAL_TASK,
      'weight' => -100,
    );
    $items['admin/nodemaker/apps/apps'] = array(
      'title' => t('NodeMaker Apps'),
      'page callback' => 'nodemaker_apps',
      'access arguments' => array('administer site configuration'),
      'file' => 'nodemaker.pages.inc',
      'type' => MENU_DEFAULT_LOCAL_TASK,
      'weight' => -100,
    );
  }

  //get enabled NodeMaker apps
  $modules = nodemaker_get_modules('apps', array(1));
  if (!empty($modules)) {
    foreach ($modules as $module => $info) {
      $info['module'] = $module;
      $items['admin/nodemaker/apps/' . $module] = array(
        'title' => t(str_replace('NodeMaker ', '', $info['name'])),
        'description' => t('Manage' . $info['name']),
        'page callback' => 'nodemaker_pages',
        'page arguments' => array($info),
        'access arguments' => array('administer site configuration'),
        'file' => 'nodemaker.pages.inc',
        'type' => MENU_LOCAL_TASK,
      );
    }
  }

  //get all apps for disable buttons
  $modules = nodemaker_get_modules('all', array(0,1));
  if (!empty($modules)) {
    foreach ($modules as $module => $info) {
      $info['module'] = $module;
      $items['admin/nodemaker/apps/' . $module . '/disable'] = array(
        'title' => "Disable " . $info['name'] . "  App",
        'page callback' => 'nodemaker_disable',
        'page arguments' => array($info),
        'access arguments' => array('administer site configuration'),
        'file' => 'nodemaker.pages.inc',
        'type' => MENU_SUGGESTED_ITEM,
      );
    }
  }

  //get all apps for enable buttons
  $modules = nodemaker_get_modules('all', array(0,1));
  if (!empty($modules)) {
    foreach ($modules as $module => $info) {
      $info['module'] = $module;
      $items['admin/nodemaker/apps/' . $module . '/enable'] = array(
        'title' => "Enable " . $info['name'] . "  App",
        'page callback' => 'nodemaker_enable',
        'page arguments' => array($info),
        'access arguments' => array('administer site configuration'),
        'file' => 'nodemaker.pages.inc',
        'type' => MENU_SUGGESTED_ITEM,
      );
    }
  }

  //get all apps for uninstall buttons
  $modules = nodemaker_get_modules('all', array(0,1));
  if (!empty($modules)) {
    foreach ($modules as $module => $info) {
      $info['module'] = $module;
      $items['admin/nodemaker/apps/' . $module . '/uninstall'] = array(
        'title' => "Uninstall " . $info['name'] . " App",
        'page callback' => 'nodemaker_uninstall',
        'page arguments' => array($info),
        'access arguments' => array('administer site configuration'),
        'file' => 'nodemaker.pages.inc',
        'type' => MENU_SUGGESTED_ITEM,
      );
    }
  }

  //Omega NodeMaker and its subthemes
  //get all themes
  $themes = list_themes();
  //get current theme
  $current = variable_get('theme_default');
  //check to see if current theme is a subtheme (infinte depth) of Omega NodeMaker
  if (in_array('Omega NodeMaker', $themes[$current]->base_themes)) {
    //provide menu item
    $omega_nodemaker_menu = TRUE;
  }
  //check to see if current theme is a subtheme (direct child) of Omega NodeMaker
  elseif ($themes[$current]->base_theme == 'omega_nodemaker') {
    //provide menu item
    $omega_nodemaker_menu = TRUE;
  }
  //check to see if Omega NodeMaker is the current theme
  elseif ($current == 'omega_nodemaker') {
    //provide menu item
    $omega_nodemaker_menu = TRUE;
  }
  if ($omega_nodemaker_menu === TRUE) {
    $items['admin/nodemaker/apps/' . $current] = array(
      'title' => $themes[$current]->info['name'],
      'description' => t('Manage !theme Theme', array('!theme' => $themes[$current]->info['name'])),
      'page callback' => 'nodemaker_pages_theme',
      'page arguments' => array($current),
      'access arguments' => array('administer themes'),
      'file' => 'nodemaker.pages.inc',
      'type' => MENU_LOCAL_TASK,
      'weight' => 100,
    );
  }

  return $items;
}




/**
 *  Implements hook_install_tasks.
 */
function nodemaker_install_tasks() {
  $tasks = array();
  $current_task = variable_get('install_task', 'done');
  
  // setup contact form settings
  $tasks['nodemaker_install_contact'] = array(
    'display_name' => st('Set contact form'),
    'display' => FALSE,
    'type' => 'normal',
  );
    
  // allow users to customize the NodeMaker installation
  $tasks['nodemaker_configure_apps'] = array(
    'display_name' => st('Configure NodeMaker'),
    'type' => 'form',
  );
  
  // run the batch process to install selected apps
  $tasks['nodemaker_install_apps'] = array(
    'display_name' => st('Installing Apps'),
    'type' => 'batch',
    'display' => strpos($current_task, 'nodemaker_') !== FALSE,
  );
  
  // run the batch process to install selected apps
  $tasks['nodemaker_install_content'] = array(
    'display_name' => st('Installing Content'),
    'type' => 'batch',
    'display' => strpos($current_task, 'nodemaker_') !== FALSE,
  );

  // finalize
  $tasks['nodemaker_finalize_install'] = array(
    'display_name' => st('Finalizing Installation'),
    'display' => strpos($current_task, 'nodemaker_') !== FALSE,
    'type' => 'batch',
  );

  return $tasks;
}

/**
 * Helper function to get all NodeMaker modules
 *
 * @param $type
 * 'all' = both apps and content modules
 * 'apps' = only apps modules
 * 'content' = only default content modules 
 *
 * @param array $status
 * 'array(1) = only enabled modules
 * 'array(0) = only disabled modules
 * 'array(0,1) = both enabled and disabled modules
 *
 * @return
 * An array, keyed by project name, of module information. 
 *
 */
function nodemaker_get_modules($type = 'all', $status = array(0,1)) {

  if ($type == 'all') {
    $query = db_select('system', 's')
      ->fields('s', array('name', 'info'))
      ->condition('name', 'nm_%', 'LIKE')
      ->condition('status', $status, 'IN')
      ->orderBy('name', 'ASC')
      ->execute();
  }
  elseif ($type == 'content') {
    $query = db_select('system', 's')
      ->fields('s', array('name', 'info'))
      ->condition('name', 'nm_%_default_content', 'LIKE')
      ->condition('status', $status, 'IN')
      ->orderBy('name', 'ASC')
      ->execute();
  }
  elseif ($type == 'apps') {
    $query = db_select('system', 's')
      ->fields('s', array('name', 'info'))
      ->condition('name', 'nm_%%', 'LIKE')
      ->condition('name', 'nm_%_default_content', 'NOT LIKE')
      ->condition('status', $status, 'IN')
      ->orderBy('name', 'ASC')
      ->execute();
  }

  $modules = array();
  while($results = $query->fetch()) {
    $modules[$results->name] = unserialize($results->info);
  }

  if (!empty($modules)) {
    return $modules;
  }
  return;
}


/*
 * Helper function to map default content nodes to the parent module
 */
function nodemaker_get_default_content() {
  //get all enabled default content modules
  $content_apps = nodemaker_get_modules('content', array(1));

  //if no content, return
  if (empty($content_apps)) {
    return array();
  }

  //get default content nodes
  $default_contents = db_select('nodemaker_default_content', 'd')
    ->fields('d', array('name', 'nid'))
    ->execute()->fetchAll();

  $mapped = array();
  //map parent modules to default content in database
  foreach($content_apps as $content_app => $info) {
    foreach($default_contents as $default_content) {
      if (is_numeric($default_content->nid)) { 
        $app = str_replace('_default_content', '', $content_app);
        if (preg_match("#$app#", $default_content->name)) {
          if ($app == 'nm_members') {
            $user = user_load($default_content->nid);
            $mapped[$app]['name'] = $info['name'];
            $mapped[$app]['content'][$default_content->nid]['machine_name'] = $default_content->name;
            $mapped[$app]['content'][$default_content->nid]['node_title'] = $user->name;
          }
          else {
            $node = node_load($default_content->nid);
            $mapped[$app]['name'] = $info['name'];
            $mapped[$app]['content'][$default_content->nid]['machine_name'] = $default_content->name;
            $mapped[$app]['content'][$default_content->nid]['node_title'] = $node->title;

          }
        }
      }
    }
  }
  return $mapped;
}


/*
 * On the Getting Started page at admin/nodemaker & admin/nodemaker/welcome
 */
function nodemaker_getting_started_form() {
  // add in custom css for nodemaker administrative pages.
  drupal_add_css(drupal_get_path('profile', 'nodemaker') . '/css/nodemaker-admin.css');
  drupal_add_js(drupal_get_path('profile', 'nodemaker') . '/js/nodemaker-admin.js'); 
  $form = array();
  // grab logo from nodemaker profile directory
  $logo = l(theme('image', array('path' => drupal_get_path('profile', 'nodemaker') . '/logo.png', 'attributes' => array('class' => array('nodemaker-logo')))), 'http://themegeeks.com', array('html' => TRUE, 'attributes' => array('target' => '_blank', 'class' => array('nodemaker-icon-link'), 'title' => 'NodeMaker by ThemeGeeks')));
  $form['nodemaker_welcome'] = array(
    '#type' => 'markup',
    '#markup' => '<div id="nodemaker-config" class="clearfix"><div id="nodemaker-intro" class="clearfix">' . $logo . '<div class="nodemaker-intro-description"><h3>'. t('Welcome to your NodeMaker website!') .'</h3><p>' . t('Your new website comes with a lot of pre-configured functionality.  We want to make sure that you learn more about each setting and explore all the features.  As you complete each task below, check it off the To-Do list.  Go explore!!  Come back to this page by clicking on "Getting Started" in the black administration menu at the top left.  Once you have finished the checklist, you will have the option to hide this page.') . '</p><p>'. t('Still looking for additional help & information? Visit the <a href="http://drupal.org/project/nodemaker" target="_blank">NodeMaker Project Page</a> or <a href="http://themegeeks.com" target="_blank">ThemeGeeks</a> for further documentation.') .'</p></div></div>',
    '#weight' => -20,
  );
  
  $form['tabs'] = array(
    '#type' => 'vertical_tabs',
    //'#prefix' => '<h2>' . t('NodeMaker Launch Checklist') . '</h2>',
  );
  
  //get all enabled modules and their getting_started content
  $modules = nodemaker_get_modules('apps', array(1));
  
  if ($modules) {
    foreach ($modules as $module => $info) {
      $function = 'nodemaker_' . $module . '_getting_started_content';
      if (function_exists($function)) {
        $form = array_merge_recursive($form, $function());  
      }
    }

  }
    

  $description1 = '<p>'.t('This is the final step!  It is time to uninstall default content before launching your website.  We know you may have chosen to edit some of the content and want to save them.  We have provided a way to do so.').'</p>';
  $description2 = '<p><em>'.t('You can save any default content as permanent by making sure it is marked as "KEEP".  You must select "UNINSTALL" for each default content app in order to complete the process.  Only content marked as KEEP will be saved on uninstall.  Content marked as "DELETE" will be permanently deleted.').'</em></p>';
  //default content fieldset
  $form['tabs']['default_content'] = array(
    '#type' => 'fieldset',
    '#title' => t('Default Content'),
    '#description' => $description1.$description2,
    '#collapsible' => TRUE,
    '#weight' => 100,
  );

  //get default content
  $default_content = nodemaker_get_default_content();
  if (!empty($default_content)) {
    //set them as checkboxes
    foreach($default_content as $module => $content) {
      $options = array();
      $default_values = array();

      if ($module == 'nm_members') {
        $link = 'user/';
      }
      else {
        $link = 'node/';
      }

      foreach($content['content'] as $nid => $data) {

        $options[$nid] = l($data['node_title'], $link.$nid, array('attributes' => array('target' => '_blank', 'title' => $data['node_title'])));
        $default_values[$nid] = $nid;

      }
      $options['disable'] = '<strong>UNINSTALL</strong> ' . $content['name'];
      $default_values['disable'] = 0;
      $form['tabs']['default_content'][$module.'_default_content'] = array(
        '#type' => 'checkboxes',
        '#title' => t($content['name']),
        '#options' => $options,
        '#default_value' => $default_values,
        // we need to run these through our own process function
        '#process' => array('nodemaker_process_defaultcontent_checkboxes'),
      );
    }
  }
  else {
    unset($form['tabs']['default_content']['#description']);
    $form['tabs']['default_content']['none'] = array(
      '#type' => 'markup',
      '#markup' => t('You currently do not have any default content apps enabled.'),
    );
  }


  //get all checkboxes
  $checkboxes = array();
  foreach ($form['tabs'] as $k => $fieldset) {
    if (is_array($fieldset)) {
      foreach ($fieldset as $checkbox => $machinename) {
        if (isset($machinename['#type']) && $machinename['#type'] == 'checkbox') {
          //for the apps
          $checkboxes[':input[name="'.$checkbox.'"]'] = array('checked' => TRUE);
        }
        if (isset($machinename['#type']) && $machinename['#type'] == 'checkboxes') {
          foreach($machinename['#options'] as $nid => $data) {
            //for the default content
            
            if (!is_numeric($nid)) {
              // only check the disable/uninstall flags
              $checkboxes[':input[name="'.$checkbox.'['.$nid.']"]'] = array('checked' => TRUE);  
            }
            
          }
        }
      }
    }
  }


  //Other Options Fieldset
  $form['tabs']['other'] = array(
    '#type' => 'fieldset',
    '#title' => t('Other Options'),
    '#description' => t('You can extend and customize NodeMaker\'s functionality with contributed modules.  We have listed a few of our favorites below.  !docs', array('!docs' => l('Learn how to download and install contributed modules.', 'http://drupal.org/documentation/install/modules-themes/modules-7', array('attributes' => array('target' => '_blank', 'title' => 'Drupal Documention'))))),
    '#collapsible' => TRUE,
    '#weight' => 101,
  );

  //add class so JS works for summary
  $form['tabs']['other']['#attributes']['class'][] = 'getting-started';

  //add summary var for JS
  drupal_add_js((array('nodemaker' => array('getting_started_summary' => array('other' => t('Modules you may want to use.'))))), 'setting');

  //make mods array
  $other_mods = array(
    'Administration' => array(
      'advanced_help' => array(
        'name' => t('Advanced Help'),
        'description' => t('Provides additional in-site documentation.  Useful when working with Views.'),
      ),
      'scheduler' => array(
        'name' => t('Scheduler'),
        'description' => t('Allows nodes to be published and unpublished on specified dates.'),
      ),
      'workbench' => array(
        'name' => t('Workbench'),
        'description' => t("Provides the ability to control who has access to edit any content based on an organization's structure.  Create a customizable editorial workflow."),
      ),
    ),
    'User Interaction' => array(
      'webform' => array(
        'name' => t('Webform'),
        'description' => t('Make survey and other forms. After a submission, users may be sent an e-mail "receipt" as well as sending a notification to administrators. Results can be exported into Excel or other spreadsheet applications. Webform also provides some basic statistical review and has and extensive API for expanding its features.'),
      ),
      'recaptcha' => array(
        'name' => t('reCAPTCHA'),
        'description' => t('Uses the reCAPTCHA web service to improve the CAPTCHA system.'),
      ),
      'mollom' => array(
        'name' => t('Mollom'),
         'description' => t('"Intelligent" spam prevention instead of using CAPTCHA.  Note that you will have to clear out the CAPTCHA form settings already in place.  !link', array('!link' => l('CAPTCHA Settings', 'admin/config/people/captcha'))),
      ),
      'apply_for_role' => array(
        'name' => t('Apply For Role'),
        'description' => t('Allows users to apply for roles and allows role administrators to approve the role applications.  Might be useful to allow users to ask to become "bloggers" on your site.'),
      ),
      'autoassignrole' => array(
        'name' => t('Auto Assign Role'),
        'description' => t('Provide an automatic assignment of roles when a new account is created and allow the end user the option of choosing their own role or roles when they create their account.  Might be useful to allow users to ask to become "bloggers" on your site.'),
      ),
      'og' => array(
        'name' => t('Organic Groups'),
        'description' => t("Enable users to create and manage their own 'groups'. Each group can have subscribers, and maintains a group home page where subscribers communicate amongst themselves."),
      ),
      'print' => array(
        'name' => t('Printer, email and PDF versions'),
        'description' => t('Allows you to generate printer-friendly versions of any page.'),
      ),
      'extlink' => array(
        'name' => t('External Links'),
        'description' => t('Using jQuery, finds all external links on a page and adds a small icon indicating it will take you offsite.'),
      ),
    ),
    'SEO' => array(
      'google_analytics' => array(
        'name' => t('Google Analytics'),
        'description' => t("Adds the Google Analytics web statistics tracking system to your website and add variables for custom tracking."),
      ),
      'seo_checklist' => array(
        'name' => t('SEO Checklist'),
        'description' => t("Uses SEO best practices to check your website for proper search engine optimization. It eliminates guesswork by creating a functional to-do list of modules and tasks that remain."),
      ),
    ),
    'Development' => array(
      'devel' => array(
        'name' => t('Devel'),
        'description' => t('Helper functions for Drupal developers and inquisitive admins.  Ability to generate placeholder content.'),
      ),
      'backup_migrate' => array(
        'name' => t('Backup & Migrate'),
        'description' => t("Simplifies the task of backing up and restoring your Drupal database."),
      ),
      'pathologic' => array(
        'name' => t('Pathologic'),
        'description' => t('Correct paths for links and images in your content.  For example, if the URL of the site changes, or the content was moved to a different server.'),
      ),
    ), 
  );

  //turn into usable markup
  $markup = '';
  foreach($other_mods as $group => $other_mod) {
    //print group header
    $markup .= '<h2>'.$group.'</h2>';
    foreach ($other_mod as $project_name => $project_details) {
      //print module name
      $markup .= '<h3>'.$project_details['name'].'</h3>';
      //print module description
      $markup .= '<p>'.$project_details['description'].'</p>';

      //make list of links
      $otherlinks = array();

      //print link to d.o.
      $otherlinks[] = l('Learn More', 'http://drupal.org/project/'.$project_name, array('attributes' => array('class' => 'nodemaker-actions', 'target' => '_blank', 'title' => $project_details['name'])));

      //find out which enable/disable link should be there - don't worry about uninstall
      $status = db_select('system', 's') 
        ->fields('s', array('info', 'status'))
        ->condition('name', $project_name)
        ->execute()->fetch();
      //turn info into something useful
      if (isset($status->info)) {
        $status->info = unserialize($status->info);
      }

      //if already enabled
      if (isset($status->status) && $status->status == 1) {
        //print link to disable
        $otherlinks[] = l('Disable', 'admin/modules', array('fragment' => strtolower(str_replace(' ', '_', $status->info['package'])), 'attributes' => array('class' => 'nodemaker-actions disable', 'title' => 'Disable '.$project_details['name'])));
      }
      //if not enabled
      elseif (isset($status->status) && $status->status == 0) {
        //print link to enable
        $otherlinks[] = l('Enable', 'admin/modules', array('fragment' => strtolower(str_replace(' ', '_', $status->info['package'])), 'attributes' => array('class' => 'nodemaker-actions enable', 'title' => 'Enable '.$project_details['name'])));
      }
      else {
        //we never downloaded it
      }
      //turn otherlinks into markup
      $markup .= theme('item_list', array('attributes' => array('class' => 'nodemaker-item-actions'), 'items' => $otherlinks));

    }
  }
  $form['tabs']['other']['modules'] = array(
    '#type' => 'markup',
    '#markup' => $markup,
  );
  

  $form['finish'] = array(
    '#type' => 'fieldset',
    '#title' => t('Finish & Hide'),
    '#collapsible' => TRUE,
    '#description' => t('Once you have marked each step as completed in the form, you will be able to hide this welcome page.'),
  );
  
  //option to hide this page if all checkboxes are checked
  $description = t('Congratulations!  You have completed all the steps.  Do you want to hide this page from showing again?');
  $form['finish']['nodemaker_hide_welcome'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide This Page'),
    '#description' => $description,
    '#default_value' => variable_get('nodemaker_hide_welcome', 0),
    '#states' => array(
      // Only show this checkbox if every step has been completed.
      'visible' => array($checkboxes),
    ),
  );

  $form['#submit'][] = 'nodemaker_getting_started_submit';
  $form['#validate'][] = 'nodemaker_getting_started_validate';

  //return the whole form
  return system_settings_form($form);
}


function nodemaker_getting_started_validate($form, &$form_state) {

}

/**
 * Submit handler
 */
function nodemaker_getting_started_submit($form, &$form_state) {
  // since we aren't ONLY submitting default content, we need to adjust the 
  // next section to determine if it's actually a default content value we
  // iterate over.
  foreach($form_state['values'] as $module => $v) {
    if(strrchr($module, 'default_content')) {
      if ($v['disable'] === 'disable') {

        foreach($v as $nid => $i) {
          //remove it from default content table
          if (is_numeric($nid)) {
            db_delete('nodemaker_default_content')
              ->condition('nid', $nid)
              ->execute();
          }
          //if content marked for deletion
          if (is_numeric($nid) && $i === 0) {
            //if nm_members default content
            if ($module == 'nm_members_default_content') {
              $pid = db_select('profile', 'p')
                ->fields('p', array('pid'))
                ->condition('uid', $nid)
                ->execute()->fetchAssoc();
              if (is_numeric($pid['pid'])) {
                $profile2 = profile2_load($pid['pid']);
                profile2_delete($profile2);
              }
              user_delete($nid);
            }
            //if regular content
            else {
              node_delete($nid);
            }
          }
          //disable the module
          module_disable(array($module));
          //uninstall it so that when enabled again, the nodes get created
          drupal_uninstall_modules(array($module));
        }
      }
    }
  }

  //hide welcome page
  if ($form_state['values']['nodemaker_hide_welcome'] == 1) {
    variable_set('nodemaker_hide_welcome', 1);
    drupal_flush_all_caches();
    drupal_set_message(t('You may enable the Welcome page again at the bottom of NodeMaker Apps.'));
  }
  return;
}


/**
 * Button to show welcome page again
 */
function nodemaker_show_welcome_form() {
  $form = array();
  $form['button'] = array(
    '#type' => 'submit',
    '#value' => t('Show Welcome Page'),
  );
  $form['#submit'] = array('nodemaker_show_welcome_submit');
  return $form;
}


/**
 * Submit handler
 */
function nodemaker_show_welcome_submit($form, &$form_state) {
  variable_set('nodemaker_hide_welcome', 0);
  drupal_flush_all_caches();
  return;
}


/**
 * Value handler
 */
function nodemaker_getting_started_checkbox_value($element, $input = FALSE, $form_state = array()) {
  //when only part of the form is displayed (per app), make sure to save the default_value for hidden elements
  
  $val = isset($element['#default_value']) ? $element['#default_value'] : 0;
  //dsm($element['#name'] . ' value: ' . $val);
  
  //dsm($element['#name'] . ' input value: ' . $input);
  if ($input !== FALSE) {
    $return = $input;
    //dsm($element);
  }
  
  if ($input === FALSE) {
    //dsm($element);
    //dsm($input);  
    //return $element['#return_value'];
    //return variable_get($element['#name'], $element['#default_value']);
    //return isset($element['#default_value']) ? $element['#default_value'] : 0;
  }
  else {
    //dsm($element);
    //dsm($input);  
    //return $input;
  }
}

function theme_toggleswitch($variables) {
  $element = $variables['element'];
  $t = get_t();
  $element['#attributes']['type'] = 'checkbox';
  element_set_attributes($element, array('id', 'name','#return_value' => 'value'));

  // Unchecked checkbox has #value of integer 0.
  if (!empty($element['#checked'])) {
    $element['#attributes']['checked'] = 'checked';
  }
  _form_set_class($element, array('form-checkbox'));
  //krumo($element);
  return '<div class="togglebox"><input' . drupal_attributes($element['#attributes']) . ' /><label for="' . $element['#id'] . '"><span></span></label></div>';
}

function nodemaker_process_defaultcontent_checkboxes($element) {
  $value = is_array($element['#value']) ? $element['#value'] : array();
  $element['#tree'] = TRUE;
  if (count($element['#options']) > 0) {
    if (!isset($element['#default_value']) || $element['#default_value'] == 0) {
      $element['#default_value'] = array();
    }
    $weight = 0;
    foreach ($element['#options'] as $key => $choice) {
      // Integer 0 is not a valid #return_value, so use '0' instead.
      // @see form_type_checkbox_value().
      // @todo For Drupal 8, cast all integer keys to strings for consistency
//   with form_process_radios().
      if ($key === 0) {
        $key = '0';
      }
      // Maintain order of options as defined in #options, in case the element
      // defines custom option sub-elements, but does not define all option
      // sub-elements.
      $weight += 0.001;

      $element += array($key => array());
      $element[$key] += array(
        '#type' => 'checkbox', 
        '#title' => $choice, 
        '#return_value' => $key, 
        '#default_value' => isset($value[$key]) ? $key : NULL, 
        '#attributes' => $element['#attributes'], 
        '#ajax' => isset($element['#ajax']) ? $element['#ajax'] : NULL, 
        '#weight' => $weight,
        '#theme' => 'toggleswitch',
      );
      if ($key == 'disable' || $key == 'uninstall') {
        // reset the uninstall option to a normal checkbox.
        $element[$key]['#theme'] = 'checkbox';
      }
    }
  }
  return $element;
}
