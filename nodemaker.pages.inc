<?php


/**
 *  Page callback for admin/nodemaker & admin/nodemaker/welcome
 */
function nodemaker_welcome() {

  return drupal_get_form('nodemaker_getting_started_form');
}


/**
 *  Page callback for admin/nodemaker/apps & admin/nodemaker/apps/apps
 */
function nodemaker_apps() {
  // add in custom css for nodemaker administrative pages.
  drupal_add_css(drupal_get_path('profile', 'nodemaker') . '/css/nodemaker-admin.css');
  //provide a way to show welcome screen again
  if (variable_get('nodemaker_hide_welcome', 0) == 1) {
    $button = drupal_get_form('nodemaker_show_welcome_form');
  }
  else {
    $button = '';
  }

  //build the verical_tabs
  $tabs['tabs'] = array(
    '#type' => 'vertical_tabs',
  );

  //get all nodemaker modules 
  $enabled_apps = nodemaker_apps_sections('apps', array(1), 'Enabled', array('disable' => 'Disable'));
  $disabled_apps = nodemaker_apps_sections('apps', array(0), 'Disabled', array('enable' => 'Enable', 'uninstall' => 'Uninstall'));
  $enabled_content = nodemaker_apps_sections('content', array(1), 'Enabled', array('uninstall' => 'Uninstall'));
  $disabled_content = nodemaker_apps_sections('content', array(0), 'Disabled', array('enable' => 'Enable'));

  //add them together
  $content = array();
  $content = array_merge_recursive($tabs, $enabled_apps, $disabled_apps, $enabled_content, $disabled_content);
  $ret = render($content) . render($button);
  return ($ret);
}


/**
 *  Helper function to build vertical tabs
 *
 * @param $type
 * 'all' = both apps and content modules
 * 'apps' = only apps modules
 * 'content' = only default content modules 
 *
 * @param array $status
 * 'array(1) = only enabled modules
 * 'array(0) = only disabled modules
 * 'array(0,1) = both enabled and disabled modules
 *
 * @param $label
 * 'Enabled'
 * 'Disabled'
 *
 * @param array $action
 * the key is the page name for the menu callback, the value is the Human readable version
 * 'disable' => 'Disable'
 * 'enable' => 'Enable'
 * 'uninstall' => 'Uninstall'
 *
 * @return
 * An renderable array of fieldsets of module information
 */
function nodemaker_apps_sections($type = 'all', $status = array(0,1), $label = 'Enabled', $action = array('disable' => 'Disabled')) {
  $content = array();
  
  //get modules
  $modules = nodemaker_get_modules($type, $status);

  //if action will be uninstalling default content, add data to modules array
  if ($type == 'content' && $action === array('uninstall' => 'Uninstall')) {
    $default_content = nodemaker_get_default_content();
    if (!empty($modules)) {
      foreach($modules as $module => $data) {
        //add information to $modules about existing default content
        if (isset($default_content[str_replace('_default_content', '', $module)])) {
          $modules[$module]['content'] = $default_content[str_replace('_default_content', '', $module)]['content'];
        }
      }
    }
  }

  if (!empty($modules)) {

    //build the items
    $items = array(); 
    foreach ($modules as $module => $info) {

      //get the icon
      $icon = drupal_get_path('module', $module) . '/' . $module . '-icon.png';
      $parent = str_replace('_default_content', '', $module);
      $parenticon = drupal_get_path('module', $parent) . '/' . $parent . '-icon.png';
      if (file_exists($icon)) {
        $image = l(theme('image', array('path' => $icon, 'attributes' => array('class' => array('nodemaker-icon')))), 'admin/nodemaker/apps/'.$module, array('html' => TRUE, 'attributes' => array('class' => array('nodemaker-icon-link'), 'title' => $info['name'])));
      }
      elseif (file_exists($parenticon)) {
        $image = l(theme('image', array('path' => $parenticon, 'attributes' => array('class' => array('nodemaker-icon')))), 'admin/nodemaker/apps/'.$parent, array('html' => TRUE, 'attributes' => array('class' => array('nodemaker-icon-link'), 'title' => $info['name'])));
      }
      else {
        $image = '';
      }

      //get the title
      if (!isset($action['disable'])) {
        $title = '<h4>' . $info['name'] . '</h4>';
      }
      else {
        $title = '<h4>' . l($info['name'], 'admin/nodemaker/apps/'.$module, array('attributes' => array('title' => $info['name']))) . '</h4>';
      }

      //get the description
      $description = $info['description'];

      //get the actions
      $actions = array();

      if (isset($action['disable'])) {
        // add an additional "configure" link to active apps
        $actions[] = l(t('Details', array('@app' => $info['name'])), 'admin/nodemaker/apps/'.$module, array('html' => TRUE, 'attributes' => array('class' => array('nodemaker-actions'), 'title' => $info['name'])));
      }

      foreach ($action as $k => $v) {
        if ($k == 'uninstall') {
          //if suppose to add an uninstall link, make sure it is uninstallable first
          include_once DRUPAL_ROOT . '/includes/install.inc';    
          // get a list of disabled & installed modules.
          $all_modules = system_rebuild_module_data();
          $disabled_modules = array();

          foreach ($all_modules as $machine_name => $module_object) {
            if (empty($module_object->status) && $module_object->schema_version > SCHEMA_UNINSTALLED) {
              $disabled_modules[$machine_name] = $module_object;
            }
          }

          //if it can be disabled, add the action link
          if (!empty($disabled_modules[$module])) {
            $actions[] = l(t($v, array('@app' => $info['name'])), 'admin/nodemaker/apps/'.$module.'/' . $k, array('attributes' => array('class' => array('nodemaker-actions', 'disable', strtolower($v)), 'title' => t($v . ' @app', array('@app' => $info['name'])))));
          }
        }
        else {
          //add the other action link
          $actions[] = l(t($v, array('@app' => $info['name'])), 'admin/nodemaker/apps/'.$module.'/' . $k, array('attributes' => array('class' => array('nodemaker-actions', strtolower($v)), 'title' => t($v . ' @app', array('@app' => $info['name'])))));
        }
      }
      
      $action_list = array(
        '#theme' => 'item_list',
        '#items' => $actions,
        '#attributes' => array(
          'class' => array(
            'nodemaker-item-actions',
          ),
        ),
      );

      //set the data
      $items[] = array(
        'data' => $image . '<div class="nodemaker-app-details">' . $title . '<p>' . $description . '</p>' . render($action_list) . '</div>',
        'class' => array('clearfix'),
      );

    }

    //label logic
    if ($type == 'apps') {
      $label2 = ' Feature';
    }
    elseif ($type == 'content') {
      $label2 = ' Content';
    }
    else {
      $label2 = '';
    }

    //set the fieldset
    $content['tabs'][$type][strtolower($label)] = array(
      '#title' => $label . $label2 . ' Apps',
      '#type' => 'fieldset',
    );


    //if action will be uninstalling default content, allow saving first
    if ($type == 'content' && $action === array('uninstall' => 'Uninstall')) {
      $content['tabs'][$type][strtolower($label)]['default_content'] = drupal_get_form('nodemaker_content_form', $modules);
    }
    //show the regular items
    else {
      $content['tabs'][$type][strtolower($label)]['data'] = array(
        '#title' => $label . $label2 . ' Apps',
        '#theme' => 'item_list',
        '#items' => $items,
        '#attributes' => array(
          'class' => array(
            'clearfix',
          ),
          'id' => array('nodemaker-apps'),
        ),
      );
    }

  }

  return $content;
}


/**
 *  Form to allow saving of content before uninstalling
 */
function nodemaker_content_form() {
  $modules = func_get_arg(2);
  if (empty($modules)) {
    return;
  }

  $form = array();
  $form['markup'] = array(
    '#type' => 'markup',
    '#markup' => '<h3>'.t('Uninstall Default Content').'</h3>'.t('Save default content as permanent before uninstalling default content app.  Only checked content will be saved on uninstall.  For any checked checkbox of an <strong>UNINSTALL</strong>, the above default content will be deleted unless checked.'),
  );

  foreach ($modules as $module => $info) {
    if (!empty($info['content'])) {
      $options = array();
      $default_values = array();

      if ($module == 'nm_members_default_content') {
        $link = 'user/';
      }
      else {
        $link = 'node/';
      }

      //get data for options
      foreach ($info['content'] as $nid => $data) {
        $options[$nid] = l($data['node_title'], $link.$nid, array('attributes' => array('target' => '_blank', 'title' => $data['node_title'])));
        $default_values[$nid] = $nid;
      }
      $options['uninstall'] = '<strong>UNINSTALL</strong> ' . $info['name'];
      $default_values['uninstall'] = 0;
      //build form element
      $form[$module] = array(
        '#type' => 'checkboxes',
        '#title' => t($info['name']),
        '#description' => t('Only content marked as "keep" will be saved on uninstall.'),
        '#options' => $options,
        '#default_value' => $default_values,
        // we need to run these through our own process function
        '#process' => array('nodemaker_process_defaultcontent_checkboxes'),
      );
    }
  }
  //add submit button
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Uninstall'),
  );
  return $form;
}


/**
 *  Submit handler for nodemaker_content_form
 */
function nodemaker_content_form_submit($form, &$form_state) {
  //disable default content modules but save content first
  foreach($form_state['values'] as $module => $v) {
    if ($v['uninstall'] === 'uninstall') {
      foreach($v as $nid => $i) {
        //remove it from default content table
        if (is_numeric($nid)) {
          db_delete('nodemaker_default_content')
            ->condition('nid', $nid)
            ->execute();
        }
        //if content marked for deletion
        if (is_numeric($nid) && $i === 0) {
          //if nm_members default content
          if ($module == 'nm_members_default_content') {
            $pid = db_select('profile', 'p')
              ->fields('p', array('pid'))
              ->condition('uid', $nid)
              ->execute()->fetchAssoc();
            if (is_numeric($pid['pid'])) {
              $profile2 = profile2_load($pid['pid']);
              profile2_delete($profile2);
            }
            user_delete($nid);
          }
          //if regular content
          else {
            node_delete($nid);
          }
        }
      }
      //disable the module
      module_disable(array($module));
      //uninstall it so that when enabled again, the nodes get created
      drupal_uninstall_modules(array($module));
    }
  }
}


/**
 *  Page callback for all admin/nodemaker/XXX pages
 */
function nodemaker_pages($module) {
  if (empty($module)) {
    return;
  }

  //if this is for the theme
  $theme = variable_get('theme_default');
  if ($module['module'] == $theme) {
    return nodemaker_pages_theme($module);
  }

  // add in custom css for nodemaker administrative pages.
  drupal_add_css(drupal_get_path('profile', 'nodemaker') . '/css/nodemaker-admin.css');
  drupal_add_js(drupal_get_path('profile', 'nodemaker') . '/js/nodemaker-admin.js'); 
  
  $content['tabs'] = array(
    '#type' => 'vertical_tabs',
    '#prefix' => '<div id="nodemaker-app-configuration">',
    '#suffix' => '</div>',
  );
 
  $content['tabs']['info'] = array(
    '#title' => 'App Info',
    '#type' => 'fieldset',
    '#attributes' => array(
      'class' => array('nodemaker-app-info'),
    ),
    '#weight' => -10,
  );

  //display the screenshot
  $screenshot = drupal_get_path('module', $module['module']) . '/' . $module['module'] . '-screenshot.png';
  if (file_exists($screenshot)) {
    $content['tabs']['info']['screenshot'] = array(
      '#group' => 'info',
      '#theme' => 'image',
      '#path' => $screenshot,
      '#alt' => $module['name'],
      '#title' => $module['name'],
      '#prefix' => '<div class="nodemaker-app">',
      '#attributes' => array(
        'class' => array('nodemaker-app-screenshot'),
      ),
    ); 
  }

  //display the description from .info
  $content['tabs']['info']['description'] = array(
    '#title' => 'Description',
    '#group' => 'info',
    '#markup' => '<p>' . $module['description'] . '</p>',
    '#prefix' => '<div class="nodemaker-app-description">',
    '#suffix' => '</div></div>',
  );
  
  //display disable link
  $content['tabs']['info']['actions'] = array(
    '#group' => 'info',
    '#markup' => l(t('Disable', array('@app' => $module['name'])), 'admin/nodemaker/apps/'.$module['module'].'/disable', array('attributes' => array('class' => array('nodemaker-actions', 'disable'), 'title' => t('Disable @app App', array('@app' => $module['name']))))),
  );
  
  //display the details
  $function = $module['module'].'_help_details';
  if (function_exists($function)) {
    //set the fieldset
    $content['tabs']['description'] = array(
      '#title' => 'Description & Features',
      '#type' => 'fieldset',
      '#weight' => -5,
    );
    //display help items
    $content['tabs']['description']['details'] = array(
      '#group' => 'info',
      '#theme' => 'item_list',
      '#prefix' => '<h3>Description</h3>',
      '#attributes' => array(
        'class' => array('nodemaker-app-help'),
      ),
      '#items' => $function(),
    );
  }

  //display getting_started launch checklist
  $getting_started = 'nodemaker_' . $module['module'] . '_getting_started_form';
  if (function_exists($getting_started)) {

    $content['tabs']['getting_started'] = array(
      '#title' => 'Launch Checklist',
      '#type' => 'fieldset',
      '#weight' => 0,
    );
    $content['tabs']['getting_started']['form'] = array(
      '#prefix' => '<h3>Launch Checklist</h3>',
      '#group' => 'config',
      '#type' => 'form',
      $getting_started => drupal_get_form($getting_started),
    );

  }  //end if getting_started function


  
  //display configuration form
  $form = 'nodemaker_'.$module['module'].'_config_form';
  if (function_exists($form)) {
    $content['tabs']['config'] = array(
      '#title' => 'Configuration',
      '#type' => 'fieldset',
      '#weight' => 0,
    );
    $content['tabs']['config']['form'] = array(
      '#prefix' => '<h3>Configurations</h3>',
      '#group' => 'config',
      '#type' => 'form',
      $form => drupal_get_form($form),
    );
  }
  
  return $content;
}



/**
 *  Page callback for all admin/nodemaker/XXX page for the theme
 */
function nodemaker_pages_theme($theme) {
  if (empty($theme)) {
    return;
  }

  // add in custom css for nodemaker administrative pages.
  drupal_add_css(drupal_get_path('profile', 'nodemaker') . '/css/nodemaker-admin.css');
  drupal_add_js(drupal_get_path('profile', 'nodemaker') . '/js/nodemaker-admin.js');

  //load theme info
  $themes = list_themes();
  $theme = $themes[$theme];

  $content['tabs'] = array(
    '#type' => 'vertical_tabs',
    '#prefix' => '<div id="nodemaker-app-configuration">',
    '#suffix' => '</div>',
  );

  $content['tabs']['info'] = array(
    '#title' => 'Theme Info',
    '#type' => 'fieldset',
    '#attributes' => array(
      'class' => array('nodemaker-app-info'),
    ),
    '#weight' => -10,
  );


  //display the screenshot
  $screenshot = $theme->info['screenshot'];
  if (!empty($screenshot)) {
    $content['tabs']['info']['screenshot'] = array(
      '#group' => 'info',
      '#theme' => 'image',
      '#path' => $screenshot,
      '#alt' => $theme->info['name'],
      '#title' => $theme->info['name'],
      '#prefix' => '<div class="nodemaker-app">',
      '#attributes' => array(
        'class' => array('nodemaker-app-screenshot'),
      ),
    );
  }

  //display the description from .info
  $content['tabs']['info']['description'] = array(
    '#title' => 'Description',
    '#group' => 'info',
    '#markup' => '<p>' . $theme->info['description'] . '</p>',
    '#prefix' => '<div class="nodemaker-app-description">',
    '#suffix' => '</div></div>',
  );

  //display the details
  $function = 'omega_nodemaker_help_details';
  $file = 'omega_nodemaker.help.inc';
  $location = drupal_get_path('theme', 'omega_nodemaker') . '/includes';
  if (file_exists($location . '/' . $file)) {
    include_once($location . '/' . $file);
  }
  if (function_exists($function)) {
    //set the fieldset
    $content['tabs']['description'] = array(
      '#title' => 'Description & Features',
      '#type' => 'fieldset',
      '#weight' => -5,
    );
    //display help items
    $content['tabs']['description']['details'] = array(
      '#group' => 'info',
      '#theme' => 'item_list',
      '#prefix' => '<h3>Description</h3>',
      '#attributes' => array(
        'class' => array('nodemaker-app-help'),
      ),
      '#items' => $function(),
    );
  }

  //display configuration form link
  $content['tabs']['config'] = array(
    '#title' => 'Configuration',
    '#type' => 'fieldset',
    '#weight' => 0,
  );
  $content['tabs']['config']['link'] = array(
    '#prefix' => '<h3>Configurations</h3>',
    '#group' => 'config',
    '#type' => 'markup',
    '#markup' => t('The @theme Theme comes with many configurations.  !link.', array('@theme' => $theme->info['name'], '!link' => l('Learn more', 'admin/appearance/settings/'.$theme->name))),
  );

  return $content;
}



/**
 * Page callback for disabling NodeMaker apps
 */
function nodemaker_disable($module) {
  //get all modules
  $all_modules = system_rebuild_module_data();
  //if this module requires anything
  if (!empty($all_modules[$module['module']]->required_by)) {
    $required_by_modules = array();
    foreach ($all_modules[$module['module']]->required_by as $name => $v) {
      //if required_by is enabled
      if (module_exists($name)) {
        $required_by_modules[] = $all_modules[$name]->info['name'];
      }
    }
  }
  //if there are any modules that require this module and is enabled
  if (!empty($required_by_modules)) {
    drupal_set_message('Please disable "'. implode(',', $required_by_modules) .'" before continuing.', 'warning');
    drupal_goto('admin/nodemaker/apps');
  }
  //go ahead and disable it
  else {
    $success = module_disable(array($module['module']));
    drupal_flush_all_caches();
    drupal_set_message('Disabled ' . $module['name']);
    drupal_goto('admin/nodemaker/apps');
  }
  return '';
}


/**
 * Page callback for enabling NodeMaker apps
 */
function nodemaker_enable($module) {
  //if this is a content mod, make sure the app is enabled first
  if (strpos($module['module'], '_default_content') > 1) {
    $app = str_replace('_default_content', '', $module['module']);
    if (!module_exists($app)) {
      module_enable(array($app));
    }
  }

  drupal_flush_all_caches();

  $success = module_enable(array($module['module']), TRUE);
  if ($success) {
    drupal_set_message('Enabled ' . $module['name']);
    if (module_exists('help')) {
      $helplink = isset($app) ? 'admin/help/'.$app : 'admin/help/'.$module['module'];
      drupal_set_message(t("Please go to !helplink to learn more about the features.", array('!helplink' => l("the help pages", $helplink))));
    }
  }
  else {
    drupal_set_message($module['name'] .' App Not Enabled');
  }
  drupal_goto('admin/nodemaker/apps');
}


/**
 * Page callback for uninstalling NodeMaker apps
 */
function nodemaker_uninstall($module) {
  require_once DRUPAL_ROOT . '/includes/install.inc';
  $success = drupal_uninstall_modules(array($module['module']));
  if ($success) {
    drupal_flush_all_caches();
    drupal_set_message('Uninstalled ' . $module['name']);
  }
  else {
    drupal_set_message('Uninstalling ' . $module['name'] . ' app failed. Please ensure all modules that depend on this module are also uninstalled.', 'warning');
  }
  drupal_goto('admin/nodemaker/apps');
}