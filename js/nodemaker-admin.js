Drupal.nodemakerAdmin = Drupal.nodemakerAdmin || {};

(function ($) {
  
  Drupal.theme.prototype.nodemakerLaunchChecklistChanged = function () {
    return '<div class="messages warning">' + Drupal.t('Changes made to these settings will not be saved until you have clicked "Save Configuration" at the bottom of the page.') + '</div>';
  };
  
  var vTabIndex;
  var totalItems;
  var activeItems;
  
  /**
   *  This function applies the summary to fieldset items in the Getting Started form.
   *  "X of Y steps completed"...
   */
  Drupal.nodemakerAdmin.initGettingStarted = function (wrapper) {    
    $(wrapper + ' .getting-started').each(function() {

      totalItems = $(this).find('input:checkbox').size();
      activeItems = $(this).find('input:checkbox:checked').size();

      //make sure we can find the correct "span.summary"
      //works for main getting started page
      vTabIndex = $(wrapper + ' .vertical-tabs-panes > fieldset').index(this);
      //works for individual app pages
      if (vTabIndex == -1) {
        var parent = $(this).parents('fieldset');
        vTabIndex = $(wrapper + ' .vertical-tabs-panes > fieldset').index(parent);
      }

      if (totalItems) {      
        $(this)
          .parents('.vertical-tabs')
          .children('ul.vertical-tabs-list')
          .children('li:eq('+vTabIndex+')')
          .find('span.summary')
          .html(activeItems + ' of ' + totalItems + ' step' + ((totalItems > 1) ? "s" : "") + ' complete.');
      }

      else {
        //try to pull the "summary" from drupal vars, or use the fieldset description
        var this_id = this.id;
        var key = this_id.replace('edit-', '');
        var summaries = Drupal.settings.nodemaker.getting_started_summary;
        var description = summaries[key];
        if (!description) {
          var description = $(this).find('.fieldset-description').html();
        }

        $(this)
          .parents('.vertical-tabs')
          .children('ul.vertical-tabs-list')
          .children('li:eq('+vTabIndex+')')
          .find('span.summary')
          .html(description);
      }

    });
  }
  
  
  // add a mobile friendly menu
  Drupal.behaviors.nodemakerCoreLabels = {
    attach: function(context, settings) { 
      // fix a touch event on iOS
      $('label').click(function() {});
    }
  };  // end Drupal.behaviors.nodemakerCoreLabels
  
  
  // show or hide descriptions based on whether it is checked
  Drupal.behaviors.nodemakerLaunchChecklist = {
    attach: function(context, settings) { 
      // main getting started form
      var gettingStartedItem = '#nodemaker-getting-started-form';
      Drupal.nodemakerAdmin.initGettingStarted(gettingStartedItem);
      
      $(gettingStartedItem + ' input:checkbox').change(function(){
        Drupal.nodemakerAdmin.initGettingStarted(gettingStartedItem);  
      });
      
      // individual app form
      var appConfigItem = '#nodemaker-app-configuration';
      Drupal.nodemakerAdmin.initGettingStarted(appConfigItem);
      
      $(gettingStartedItem + ' input:checkbox').change(function(){
        Drupal.nodemakerAdmin.initGettingStarted(appConfigItem);  
      });
      
      // hide description for done items
      $('.getting-started :checkbox:checked')
        .parents('.form-type-checkbox')
        .find('.description')
        .css('display', 'none');
      
      // toggle description on items when clicked.
      $('.getting-started :checkbox').click(function(){
        $(this)
          .parents('.form-type-checkbox')
          .find('.description')
          .slideToggle('fast');
      });
    }
  };  // end Drupal.behaviors.nodemakerCoreLabels
  
  // add a mobile friendly menu
  Drupal.behaviors.nodemakerContentChecklist = {
    attach: function(context, settings) { 
      //var vTabIndex = $('#nodemaker-app-configuration .vertical-tabs-panes fieldset').index(this);
      var fieldset = $('#nodemaker-config #edit-default-content');
      var fieldsetIndex = $('#nodemaker-config .vertical-tabs-panes fieldset').index(fieldset);
      
      var contentModules = $('#nodemaker-config #edit-default-content .form-type-checkboxes').size();
      var contentItems = $('#nodemaker-config #edit-default-content').find('input:checkbox').size();
      contentItems = contentItems - contentModules;
      
      $('#nodemaker-config ul.vertical-tabs-list li:eq('+fieldsetIndex+')')
        .find('span.summary')
        .html(contentItems + ' sample nodes in ' + contentModules + ' content apps.');
      
      //console.log(contentModules);
    }
  };  // end Drupal.behaviors.nodemakerCoreLabels
  
  // add a dsm for form changes
  Drupal.behaviors.nodemakerAppChecklistChanged = {
    attach: function(context, settings) { 
      var alreadyWarned = false;

      //config form on individual app page (both checkbox and radio)
      //getting started launch checklist on individual app page
      //getting started launch checklist on main getting started page
      $('#nodemaker-app-configuration .nodemaker-app-config input:checkbox, #nodemaker-app-configuration .nodemaker-app-config input:radio, #nodemaker-app-configuration .getting-started input:checkbox, #nodemaker-getting-started-form .getting-started input:checkbox').change(function() {
        
        var message = $(Drupal.theme('nodemakerLaunchChecklistChanged'));
        var messageLocation = $(this).parents('.vertical-tabs-panes').children('fieldset:first-child');
        if (!alreadyWarned) {
          message.insertBefore(messageLocation).hide().fadeIn('slow');
          alreadyWarned = true;
        }
        
      });

    }
  };  // end Drupal.behaviors.nodemakerCoreLabels
  
  
  
  
  
})(jQuery);