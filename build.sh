#!/bin/sh

# Script to build a ThemeGeeks NodeMaker site
#
# There is an assumption that the directory being built to exists outside of the
# directory containing the working copy of the repo. So, ./build.sh ../html rather
# than ./build.sh html (building inside the repo leads to recursion)
#

set -e # bail on a failed command

usage () {
  echo "Usage $0 target_build_dir [link]"
  echo "See comments at top of script for more information about script functionality"
}


# Make sure the correct number of args was passed from the command line
if [ $# -eq 0 ]; then
  usage
  exit 1
fi

MAKEFILE='nodemaker.make'

# Initialize variables based on target environment
DRUSH_OPTS='--working-copy'
TARGET=$1

# Make sure we have a target directory
if [ -z "$TARGET" ]; then
  usage
  exit 2
fi

CALLPATH=`dirname $0`
ABS_CALLPATH=`cd $CALLPATH; pwd`

# Do the build
drush make $DRUSH_OPTS $CALLPATH/$MAKEFILE $TARGET
echo $TARGET
DRUPAL=`cd $TARGET; pwd`

ln -s $ABS_CALLPATH $DRUPAL/profiles/nodemaker

if [ "$2" = "link" ]; then
  #cd $DRUPAL/sites/default && ln -s ../../../settings.php && ln -s ../../../files
  cd $DRUPAL/sites/default && ln -s ../../../settings.php && mkdir files
fi

function drupalrepos {
  # Setup alternate remotes for nodemaker apps on drupal.org after initial GitHub checkouts
  nodemaker[0]='nm_announcements'
  nodemaker[1]='nm_blog'
  nodemaker[2]='nm_core'
  nodemaker[3]='nm_events'
  nodemaker[4]='nm_galleries'
  nodemaker[5]='nm_reviews'
  nodemaker[6]='nm_testimonials'
  nodemaker[7]='nm_teams'
  nodemaker[8]='nm_socialnetwork'
  
  
  # loop the apps and add the new remote for the appropriate user
  for app in ${nodemaker[@]} 
  do
    cd $DRUPAL/sites/all/modules/apps/$app && git remote add drupal $USER@git.drupal.org:project/$app.git && git reset --hard
    echo "Added remote: $USER@git.drupal.org:project/$app.git to the $app app."
  done
  
  nodemakerthemes[0]='omega_nodemaker'
  
  # loop the themes and add the new remote for the appropriate user
  for theme in ${nodemakerthemes[@]} 
  do
    cd $DRUPAL/sites/all/themes/$theme && git remote add drupal $USER@git.drupal.org:project/$theme.git && git reset --hard
    echo "Added remote: $USER@git.drupal.org:project/$theme.git to the $theme theme."
  done
  
  # add in a git reset for the nodemaker_content module
  # cd $DRUPAL/sites/all/modules/content && git reset --hard
}


  
echo "Would you like to setup git remotes on Drupal.org in addition the GitHub default repositories?"

select yn in "himerus" "miche" "No"; do
    case $yn in
        himerus ) USER='himerus' drupalrepos; break;;
        miche ) USER='miche' drupalrepos; break;;
        No ) echo "Okay then... I guess you will only be committing to GitHub then."; exit;;
    esac
done
